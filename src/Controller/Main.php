<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Tests\Controller\TestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Main extends Controller
{
    public static $dbSettings = [
        'username' => 'root',
        'password' => 'root',
        'hostname' => '127.0.0.1',
    ];

    public function addEmail(Request $request)
    {
        try {
            $types = implode('|', $request->get('type'));
            $db = new \PDO(sprintf("mysql:host=%s", Main::$dbSettings['hostname']), Main::$dbSettings['username'], Main::$dbSettings['password']);
            $db->exec("use mail; select @id:=max(id)+1 from mail_queue; INSERT INTO mail_queue SET created_at=current_timestamp(), status='new', id=@id, email='{$request->request->get('email')}', subject='{$request->request->get('subject')}', mail = '{$request->request->get('message')}', types = '{$types}'");

            return new JsonResponse(['success' => true, 'id' => $db->query('select max(id) from mail_queue')->fetchColumn()]);
        } catch (\Throwable $exception) {
            return new Response($exception->getMessage());
        }
    }

    public function getEmail($id)
    {
        $db = new \PDO(sprintf("mysql:host=%s;dbname=mail", Main::$dbSettings['hostname']), Main::$dbSettings['username'], Main::$dbSettings['password']);
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        if ($id == 0) {
            return new JsonResponse(['success' => true, 'body' => $db->query("select * from mail_queue")->fetchAll(\PDO::FETCH_ASSOC)]);
        }

        if (empty($db->query("select * from mail_queue where id = " . $id)->fetch(\PDO::FETCH_ASSOC))) {
            return new Response('Nothing found');
        }

        return new JsonResponse(['success' => true, 'body' => $db->query("select * from mail_queue where id = " . $id)->fetch(\PDO::FETCH_ASSOC)['mail']]);
    }

    public function getConnection()
    {
        $db = new \PDO(sprintf("mysql:host=%s;dbname=mail", Main::$dbSettings['hostname']), Main::$dbSettings['username'], Main::$dbSettings['password']);
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $db;
    }

    public function getStatus($id)
    {
        $db = $this->getConnection();

        $status = $db->query("select `status` from mail_queue where id = " . $id)->fetch();

        if ($status) {
            return new JsonResponse(['success' => true, 'status' => $status]);
        }

        return new Response('Message not found');
    }

    public function updateStatus($id, $status)
    {
        $db = $this->getConnection();

        if ($db->exec("update mail_queue set `status` = '$status' where id = " . $id)) {
            return new JsonResponse(['success' => true, 'status' => $status]);
        }

        return new Response('Failed to retrieve mail');
    }
}
